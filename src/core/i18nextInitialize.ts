import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import locales from 'locales'

const i18nextInitialize = () => i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    ns: Object.keys(locales.es),
    defaultNS: 'common',
    resources: locales,
    lng: 'es',
    fallbackLng: 'es',
    interpolation: {
      escapeValue: false
    }
  })

export default i18nextInitialize
