export const config = {}

export const constants = {
  AUTH_TOKEN_KEY: 'simplemonkey::token',
  SIMPLEMONKEY_API_URI: process.env.SIMPLEMONKEY_API_URI || 'http://localhost:4000'
}
