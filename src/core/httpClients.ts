/* eslint-disable import/prefer-default-export */
import axios from 'axios'
import { constants } from './config'

interface ClientProps {
  token?: string
}

const getAuthorizationHeader = (token?: string) => (
  token ? `Bearer ${token}` : undefined
)

export const getSimpleMonkeyClient = (props?: ClientProps) => axios.create({
  baseURL: constants.SIMPLEMONKEY_API_URI,
  headers: {
    Authorization: getAuthorizationHeader(props?.token)
  }
})
