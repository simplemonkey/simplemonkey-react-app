import React, { Suspense } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { QueryClient, QueryClientProvider } from 'react-query'
import { DefaultLayout } from 'components/layout'
import RouterSwitch from './components/RouterSwitch'

const SuspensePage = () => {
  return (
    <p>Loading lazy component</p>
  )
}

const queryClient = new QueryClient()

const Router: React.FC = () => {
  return (
    <BrowserRouter>
      <QueryClientProvider client={queryClient}>
        <DefaultLayout>
          <Suspense fallback={<SuspensePage />}>
            <RouterSwitch />
          </Suspense>
        </DefaultLayout>
      </QueryClientProvider>
    </BrowserRouter>
  )
}

export default Router
