import React from 'react'
import { RecoilRoot } from 'recoil'
import { ChakraProvider } from '@chakra-ui/react'
import Router from 'core/Router'
import i18nextInitialize from 'core/i18nextInitialize'

i18nextInitialize()

function App() {
  return (
    <ChakraProvider>
      <RecoilRoot>
        <Router />
      </RecoilRoot>
    </ChakraProvider>
  )
}

export default App
