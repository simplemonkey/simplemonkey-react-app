import { AxiosInstance } from 'axios'
import { getSimpleMonkeyClient } from 'core/httpClients'

export interface MovementDTO {
  createdAt: number
  updatedAt: number
  id: string
  uid: string
  name: string
  description: string
  date: string
  amount: number
  currency: string
  income: boolean
  done: boolean
  coordinates: string
  feeNumber: number
  payday: number
}

export interface BalanceDTO {
  expense: number
  income: number
}

export class MovementService {
  readonly httpClient: AxiosInstance

  readonly uid?: string

  constructor(uid?: string, token?: string) {
    this.uid = uid
    this.httpClient = getSimpleMonkeyClient({ token })
  }

  getBalance = async () => {
    return this.httpClient.get<BalanceDTO>(`/movements/${this.uid}/balance`)
  }

  getLastSixMonths = async () => {
    return this.httpClient.get<MovementDTO[]>(`/movements/${this.uid}/last-six-months`)
  }

  getLastMovements = async () => {
    return this.httpClient.get<MovementDTO[]>(`/movements/${this.uid}/last-ten`)
  }
}
