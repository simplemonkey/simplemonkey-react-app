import { AxiosInstance } from 'axios'
import { getSimpleMonkeyClient } from 'core/httpClients'

export interface LoginDTO {
  email: string
  password: string
}

export interface RegisterDTO {
  firstName: string
  lastName: string
  email: string
  password: string
}

export class AuthService {
  readonly httpClient: AxiosInstance = getSimpleMonkeyClient()

  login = async (user: LoginDTO) => {
    return this.httpClient.post('/auth/login', user)
  }

  register = async (user: RegisterDTO) => {
    return this.httpClient.post('/auth/register', user)
  }
}
