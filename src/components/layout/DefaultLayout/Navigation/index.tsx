import React from 'react'
import { Text, VStack, Spacer } from '@chakra-ui/react'
import { faChartPie, faCog, faList, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { useAuth } from 'hooks'
import NavigationLink from './NavigationLink'
import NavigationButton from './NavigationButton'

const Navigation = () => {
  const { logout } = useAuth()
  return (
    <VStack
      as="nav"
      bgGradient="linear(to-br, yellow.500, yellow.400)"
      color="white"
      w="80"
      flexShrink={0}
      h="100vh"
      px="4"
      py="6"
      userSelect="none"
      alignItems="stretch">
      <Text textAlign="center" py="4" fontWeight="bold" fontSize="2xl">
        SimpleMonkey
      </Text>
      <NavigationLink to="/" icon={faChartPie} label="Dashboard" />
      <NavigationLink to="/page" icon={faList} label="Mis Movimientos" />
      <NavigationLink to="/page" icon={faCog} label="Mi Cuenta" />
      <NavigationButton onClick={logout} icon={faSignOutAlt} label="Cerrar Sesión" />
      <Spacer />
      <Text textAlign="center">v0.1.0 · 2021</Text>
    </VStack>
  )
}

export default Navigation
