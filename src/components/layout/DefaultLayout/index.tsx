import React, { ReactElement } from 'react'
import { Flex } from '@chakra-ui/react'
import { useAuth } from 'hooks'
import Navigation from './Navigation'

interface DefaultLayoutProps {
  children?: ReactElement
}

const DefaultLayout = ({ children }: DefaultLayoutProps) => {
  const { isAuthenticated } = useAuth()
  return (
    <Flex as="main" justifyContent="stretch" w="full" minH="100vh" h="full" overflow="hidden">
      {isAuthenticated && <Navigation />}
      {children}
    </Flex>
  )
}

export default DefaultLayout
