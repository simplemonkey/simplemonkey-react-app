import React from 'react'
import { useTranslation } from 'react-i18next'
import { Button } from '@chakra-ui/button'
import { VStack } from '@chakra-ui/layout'
import { HookFormControl } from 'components/common'
import useLogin from '../hooks/useLogin'

const LoginForm = () => {
  const { register, handleSubmit, isLoading, errors } = useLogin()
  const { t } = useTranslation()

  return (
    <VStack as="form" onSubmit={handleSubmit} spacing="6" mb="6" alignItems="flex-start">
      <HookFormControl
        label={t('auth:email_label')}
        fieldName="email"
        placeholder={t('auth:email_placeholder')}
        register={register}
        error={errors.email} />
      <HookFormControl
        label={t('auth:password_label')}
        fieldName="password"
        type="password"
        register={register}
        error={errors.password} />
      <Button type="submit" colorScheme="purple" isDisabled={isLoading}>
        {t('auth:login_button')}
      </Button>
    </VStack>
  )
}

export default LoginForm
