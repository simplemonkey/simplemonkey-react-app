import { useMemo } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import { yupResolver } from '@hookform/resolvers/yup'
import { useToast } from '@chakra-ui/toast'
import { useAuth } from 'hooks'
import { AuthService } from 'services'
import loginSchema from '../validations/loginSchema'

const useLogin = () => {
  const authService = useMemo(() => new AuthService(), [])
  const { register, handleSubmit, formState: { errors } } = useForm({
    resolver: yupResolver(loginSchema)
  })
  const { mutateAsync: execLogin, isLoading } = useMutation(authService.login)
  const { setAuthToken } = useAuth()

  const toast = useToast()

  const onSubmit = async (formData: { email: string, password: string }) => {
    try {
      const { data } = await execLogin(formData)
      setAuthToken(data.idToken)
    } catch (e) {
      toast({
        title: e.message,
        status: 'error',
        isClosable: true
      })
    }
  }

  return {
    isLoading,
    errors,
    register,
    handleSubmit: handleSubmit(onSubmit)
  }
}

export default useLogin
