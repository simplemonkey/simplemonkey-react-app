import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { useQuery } from 'react-query'
import { isAfter, subMonths } from 'date-fns'
import { useAuth } from 'hooks'
import { MovementDTO, MovementService } from 'services/MovementService'

const Dashboard = () => {
  const { t } = useTranslation()
  const { decodedToken, token } = useAuth()

  const service = useMemo(
    () => new MovementService(decodedToken?.user_id, token as string),
    [decodedToken, token]
  )

  const { data: balanceData } = useQuery('balance', service.getBalance)
  const { data: lastMovementsData } = useQuery('lastMovements', service.getLastMovements)
  const { data: lastSixMonthsData } = useQuery('lastSixMonth', service.getLastSixMonths)

  const lastMonthData = useMemo(() => (
    lastSixMonthsData?.data?.filter(
      (movement: MovementDTO) => isAfter(new Date(movement.date), subMonths(new Date(), 1))
    )
  ), [lastSixMonthsData])

  return (
    <section>
      <p>{t('test')}</p>
    </section>
  )
}

export default Dashboard
