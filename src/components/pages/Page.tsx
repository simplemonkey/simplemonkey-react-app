import React from 'react'

const Page: React.FC = () => {
  return (
    <section>
      <p>
        Lorem ipsum dolor sit amet consectetur adipiscing elit proin risus,
        inceptos pretium penatibus lectus suscipit phasellus a sapien, ultrices
        lacus blandit diam condimentum feugiat id neque. Pellentesque erat
        inceptos id nisl netus sem gravida imperdiet fames, cursus nec sed
        dictumst justo senectus lobortis quisque purus, vestibulum quis bibendum
        curae interdum est mattis lacus. Magna phasellus faucibus sociosqu metus
        netus parturient nullam, fusce himenaeos eget cum maecenas facilisi
        magnis, blandit iaculis tellus sagittis semper sem. Per sed arcu tellus
        dis tristique metus porta, nulla fringilla feugiat laoreet curabitur
        potenti, cras mus rhoncus pulvinar tortor faucibus.
      </p>
    </section>
  )
}

export default Page
