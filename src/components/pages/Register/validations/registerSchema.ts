import { date, object, ref, string } from 'yup'

const registerSchema = object().shape({
  firstName: string()
    .min(3)
    .max(20)
    .required(),
  lastName: string()
    .min(3)
    .max(20)
    .required(),
  email: string()
    .email()
    .required(),
  birth: date()
    .max(new Date())
    .required(),
  password: string()
    .min(6)
    .required(),
  passwordRepeat: string()
    .oneOf([ref('password')], 'Las contraseñas deben coincidir')
})

export default registerSchema
