/* eslint-disable react/jsx-props-no-spreading */
import React from 'react'
import { Link as RouterLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Box, Flex, Link } from '@chakra-ui/layout'
import RegisterForm from './components/RegisterForm'
import RegisterTitle from './components/RegisterTitle'

const Register: React.FC = () => {
  const { t } = useTranslation()
  return (
    <Flex justifyContent="stretch">
      <Flex as="section" flex="0.5" py="12" px="24" flexDir="column" justifyContent="center">
        <RegisterTitle />
        <RegisterForm />
        <Link as={RouterLink} to="/login" opacity=".75">
          {t('auth:login_link')}
        </Link>
      </Flex>
      <Box
        as="section"
        flex="1"
        bgGradient="linear(to-br, yellow.500, yellow.400)"
        minH="100vh" />
    </Flex>
  )
}

export default Register
