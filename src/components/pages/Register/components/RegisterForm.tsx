/* eslint-disable react/jsx-props-no-spreading */
import React from 'react'
import { useTranslation } from 'react-i18next'
import { Button } from '@chakra-ui/button'
import { VStack } from '@chakra-ui/layout'
import { HookFormControl } from 'components/common'
import useRegister from '../hooks/useRegister'

const RegisterForm = () => {
  const { register, handleSubmit, isLoading, errors } = useRegister()
  const { t } = useTranslation()

  return (
    <VStack as="form" onSubmit={handleSubmit} spacing="6" mb="6" alignItems="flex-start">
      <HookFormControl
        label={t('auth:email_label')}
        fieldName="email"
        placeholder={t('auth:email_placeholder')}
        register={register}
        error={errors.email} />
      <HookFormControl
        label={t('auth:firstname_label')}
        fieldName="firstName"
        placeholder={t('auth:firstname_placeholder')}
        register={register}
        error={errors.firstName} />
      <HookFormControl
        label={t('auth:lastname_label')}
        fieldName="lastName"
        placeholder={t('auth:lastname_placeholder')}
        register={register}
        error={errors.lastName} />
      <HookFormControl
        label={t('auth:birth_label')}
        fieldName="birth"
        type="date"
        register={register}
        error={errors.birth} />
      <HookFormControl
        label={t('auth:password_label')}
        fieldName="password"
        type="password"
        register={register}
        error={errors.password} />
      <HookFormControl
        label={t('auth:password_repeat_label')}
        fieldName="passwordRepeat"
        type="password"
        register={register}
        error={errors.passwordRepeat} />
      <Button type="submit" colorScheme="purple" isDisabled={isLoading}>
        {t('auth:register_button')}
      </Button>
    </VStack>
  )
}

export default RegisterForm
