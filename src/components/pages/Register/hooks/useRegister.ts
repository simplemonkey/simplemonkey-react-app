import { useMemo } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import { useHistory } from 'react-router'
import { useToast } from '@chakra-ui/toast'
import { yupResolver } from '@hookform/resolvers/yup'
import { AuthService } from 'services'
import registerSchema from '../validations/registerSchema'

interface RegisterFormData {
  email: string
  password: string
  firstName: string
  lastName: string
  birth: string
}

const useRegister = () => {
  const authService = useMemo(() => new AuthService(), [])
  const { push } = useHistory()
  const { register, handleSubmit, formState: { errors } } = useForm({
    resolver: yupResolver(registerSchema)
  })
  const { mutateAsync: execRegister, isLoading } = useMutation(authService.register)

  const toast = useToast()

  const onSubmit = async (formData: RegisterFormData) => {
    try {
      await execRegister(formData)
      toast({
        title: 'Tu cuenta ha sido creada con éxito',
        status: 'success',
        isClosable: true
      })
      push('/login')
    } catch (e) {
      toast({
        title: e.message,
        status: 'error',
        isClosable: true
      })
    }
  }

  return {
    errors,
    isLoading,
    register,
    handleSubmit: handleSubmit(onSubmit)
  }
}

export default useRegister
