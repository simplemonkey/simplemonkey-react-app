import React from 'react'

export interface Page {
  path: string
  exact: boolean
  component: React.LazyExoticComponent<React.FC>
  public?: boolean
}

const pages: Page[] = [
  {
    path: '/',
    exact: true,
    component: React.lazy(() => import('./Dashboard'))
  },
  {
    path: '/page',
    exact: true,
    component: React.lazy(() => import('./Page'))
  },
  {
    path: '/login',
    exact: true,
    public: true,
    component: React.lazy(() => import('./Login'))
  },
  {
    path: '/register',
    exact: true,
    public: true,
    component: React.lazy(() => import('./Register'))
  },
  {
    path: '*',
    exact: false,
    component: React.lazy(() => import('./NotFound'))
  }
]

export default pages
