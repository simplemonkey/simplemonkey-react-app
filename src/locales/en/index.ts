import auth from './auth.json'
import backend from './backend.json'
import common from './common.json'
import navigation from './navigation.json'
import validations from './validations.json'

export default {
  auth,
  backend,
  common,
  navigation,
  validations
}
