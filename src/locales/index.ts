import es from './es'
import en from './en'

const locales = {
  es,
  en
}

export default locales
